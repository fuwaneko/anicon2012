require 'sinatra/base'
require 'oauth'

require 'uri'
require 'mongo'

require 'kramdown'

# db config
mongo_url = ENV['MONGOLAB_URI'] || 'mongodb://localhost/sashimi'
conn = Mongo::Connection.from_uri(mongo_url)
$db = conn.db(URI.parse(mongo_url).path.gsub(/^\//, ''))

# twitter oauth config
raise "Please set TWITTER_OAUTH_KEY and TWITTER_OAUTH_SECRET environment variables" unless $twitter_key = ENV['TWITTER_OAUTH_KEY'] and $twitter_secret = ENV['TWITTER_OAUTH_SECRET']

class AniCon < Sinatra::Base

    # sinatra settings
    enable :sessions

    not_found do
        "Not found"
    end

    #helpers
    helpers do
        def logged_in?
            user = session['access_token']
            if user
                return true
            end 
            false
        end
        
        def sensei?
            user = session['access_token']
            return user && ['sotona', 'Wounkun'].include?(user.params['screen_name'])
        end
    end

    set(:sensei) { |value| condition { sensei? == value } }
    set(:auth) { |value| condition { logged_in? == value } }

    get '/' do
        items = $db['news'].find({}, {:sort => ["created_at", :desc], :limit => 3})
        erb :index, :locals => { :latest_news => items }
    end

    get '/news' do
        items = $db['news'].find({}, {:sort => ["created_at", :desc]})
        erb :news, :locals => {:items => items}
    end

    get '/news/:id' do
        item = $db['news'].find_one({"slug" => params[:id]})
        erb :news_item, :locals => {:item => item}
    end

    get '/elections' do
        erb :elections
    end

    get '/prizes' do
        erb :prizes
    end

    get '/auth' do
        if session['access_token']; redirect '/'; end
        consumer = OAuth::Consumer.new $twitter_key, $twitter_secret,
        {
            :site => "https://api.twitter.com/",
            :scheme => :header,
            :request_token_path => "/oauth/request_token",
            :access_token_path => "/oauth/access_token",
            :authorize_path => "/oauth/authenticate"
        }

        request_token = consumer.get_request_token :oauth_callback => url("/auth/twitter")
        session['request_token'] = request_token
        redirect request_token.authorize_url
    end

    get '/auth/twitter' do
        session['access_token'] = session['request_token'].get_access_token
        redirect '/'
    end

    # admin

    get '/dojo', :sensei => true do
        erb :"dojo/index"
    end

    get '/dojo/news', :sensei => true do
        news = $db["news"].find({})
        erb :"dojo/news", :locals => {:news => news}
    end

    post '/dojo/news/add', :sensei => true do
        doc = params[:form]
        doc["created_at"] = Time.now.getutc
        $db["news"].insert(doc)
        redirect '/dojo/news'
    end

    get '/dojo/news/edit/:id', :sensei => true do
        item = $db['news'].find_one({"slug" => params[:id]})
        erb :'dojo/news_edit', :locals => { :item => item }
    end

    post '/dojo/news/edit/:id', :sensei => true do
        _id = BSON::ObjectId::from_string(params[:id])
        doc = $db['news'].find_one({"_id" => _id})
        params[:form]['created_at'] = doc['created_at']
        $db['news'].update({"_id" => _id}, params[:form])
    end

    post '/dojo/news/del', :sensei => true do
        if params[:ids].respond_to?(:each)
            params[:ids].each do |id|
              $db['news'].remove({'_id' => BSON::ObjectId::from_string(id)})
            end
            "Ok"
        else
            "Nothing selected"
        end
    end
end